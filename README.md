# vegLab API

---

L'[API](https://fr.wikipedia.org/wiki/Interface_de_programmation) web fait partie intégrante de la 
[pile applicative vegLab](https://gitlab.com/veglab/veglab-stack).  

Il s'agit d'une API [REST](https://fr.wikipedia.org/wiki/Representational_state_transfer) basée sur le framework 
[Symfony](https://symfony.com/) et plus particulièrement sa distribution [API Platform](https://api-platform.com/).  

L'utilisation de la distribution API Platform nous permet de bénéficier d'un outil adapté aux standards du web, 
connu, documenté, maintenu et libre.

Citons notamment le respect de l'architecture REST, l'utilisation du format [JSON-LD](https://json-ld.org/), 
le support d'Elasticsearch, des [SSE](https://developer.mozilla.org/fr/docs/Web/API/Server-sent_events/Using_server-sent_events), 
une bonne gestion des processus de (dé)sérialisation des données, la gestion du cache ainsi que l'ensemble des solutions fournies 
par Symfony. Il est ausi doté d'une [documentation bien conçue](https://api-platform.com/docs). 

---

L'API est en lien avec une base de données [PostgreSQL](https://www.postgresql.org/) (ainsi que l'extension [PostGIS](https://postgis.net/)).
Une partie des données (notamment les tableaux) est dupliquée dans une instance [Elasticsearch](https://www.elastic.co/fr/elasticsearch/).

L'utilisation de ces deux technologies permet de bénéficier de la robustesse de PostgreSQL ainsi que de la rapidité de
recherche d'Elasticsearch.

> PostgreSQL est largement reconnu pour son comportement stable [...]
> Outre sa capacité à gérer des bases de données volumineuses, PostgreSQL est souvent utilisé pour sa capacité à
> gérer des bases de données spatiales (SIG), grâce à son extension PostGIS.  
> -- _[wikipedia](https://fr.wikipedia.org/wiki/PostgreSQL)_

> Elasticsearch permet de faire des recherches sur tout type de document.
> Il possède une architecture adaptable, fait des recherches quasiment en temps réel.  
> -- _[wikipedia](https://fr.wikipedia.org/wiki/Elasticsearch)_

## Installation
Cette API n'a pas vocation à être déployée indépendamment de la stack vegLab.  

Suivre la procédure indiquée dans la section d'installation de la [documentation principale de vegLab](https://gitlab.com/veglab/veglab-stack#installation).  

## Utilisation
### Accès à l'interface "Swagger" 
API Platform intègre par défaut [Swagger](https://swagger.io/).  

> Swagger est un langage de description d'interface permettant de décrire des API exprimées à l'aide de JSON.  
> Il est utilisé avec toute une série d'outils logiciels open source pour concevoir, créer, documenter et utiliser des services Web.  
> -- _[wikipedia](https://fr.wikipedia.org/wiki/Swagger_(logiciel))_

L'URL d'accès à l'API est donnée dans la variable d'environnement [__TODO__] du projet [veglab-stack](https://gitlab.com/veglab/veglab-stack).  

L'accès par un navigateur renvoie directement vers une version HTML de la documentation Swagger. Les entités diffusées, 
leurs propriétés et formats ainsi que les différentes méthodes exposées y sont consultables.

![swagger](./docs/img/swagger_html.png)

### Utilisation des verbes HTTP

| Verbe                                                                        | Utilisation dans vegLab                                   |
|------------------------------------------------------------------------------|-----------------------------------------------------------|
| [GET](https://developer.mozilla.org/fr/docs/Web/HTTP/M%C3%A9thode/GET)       | Récupérer une ou plusieurs ressources.                    |
| [POST](https://developer.mozilla.org/fr/docs/Web/HTTP/M%C3%A9thode/POST)     | Créer une ressource.                                      |
| [PUT](https://developer.mozilla.org/fr/docs/Web/HTTP/M%C3%A9thode/PUT)       | Remplacer une ressource existante (dans son intégralité). |
| [PATCH](https://developer.mozilla.org/fr/docs/Web/HTTP/M%C3%A9thode/PATCH)   | Modifier une ressource.                                   |
| [DELETE](https://developer.mozilla.org/fr/docs/Web/HTTP/M%C3%A9thode/DELETE) | Supprimer une ressource.                                  |
_Certaines modifications depuis le client web vegLab utilisent parfois la méthode PUT pour modifier une ressource, 
lorsque les modifications sont importantes. C'est notamment le cas  lors de la modification/sauvegarde des tableaux (un seul PUT plutôt que plusieurs PATCH)._

### Modèle de données
Voir la [documentation dédiée](./docs/veglab_model.md).

## Elasticsearch

## Sécurité

## Tests
