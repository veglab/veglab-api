# Modèle de données vegLab

Le modèle de données suit les considérations métiers du phytosociologue. Il est centré autour du **tableau phytosociologique**.  

Nous présentons ici les principaux concepts utilisés ainsi que le modèle de données partiel. Le modèle complet peut être 
retrouvé directement en base de données, dans la base de code ou via l'interface Swagger de l'API.

## Anatomie d'un tableau phytosociologique dans vegLab


## Termes et définitions
### Idiotaxon
### Synusie
### Microcénose
### (Phytocénose)
### Tesela
### Catena

## Modèles partiels

